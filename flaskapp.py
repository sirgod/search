#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, Markup, url_for
from flask_sslify import SSLify
from bs4 import BeautifulSoup
from urlparse import urlparse, parse_qs
import requests
import os

app = Flask(__name__)
sslify = SSLify(app, permanent=True)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))


def extract_url(link):
    try:

        # Valid results are absolute URLs not pointing to a Google domain
        # like images.google.com or googleusercontent.com
        o = urlparse(link, 'http')
        if o.netloc and 'google' not in o.netloc:
            return link

        # Decode hidden URLs.
        if link.startswith('/url?'):
            link = parse_qs(o.query)['q'][0]

            # Valid results are absolute URLs not pointing to a Google domain
            # like images.google.com or googleusercontent.com
            o = urlparse(link, 'http')
            if o.netloc and 'google' not in o.netloc:
                return link

    # Otherwise, or on error, return None.
    except Exception:
        pass
    return None


def get_page(keywords, page=1, search_type=None):
    """return the search div"""
    base_url = "https://www.google.com/search"
    payload = {"q": keywords, "hl": "en", "btnG": "Google Search", "safe": "off", "tbs": "0"}
    if search_type == "images":
        payload["tpe"] = "isch"
        base_url = "https://www.google.com/images"
    headers = {"User-Agent": 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)'}
    if page > 1:
        payload["start"] = (page-1) * 10
    if app.debug:
            path = os.path.join(APP_ROOT, "templates/test.html")
            with open(path) as html:
                soup = BeautifulSoup(html)
    else:
        res = requests.get(base_url, params=payload, headers=headers)
        soup = BeautifulSoup(res.text)
    search_div = soup.find(id='search')
    search_div.ol.name = "ul"
    anchors = search_div.findAll('a')
    for a in anchors:

        # Leave only the "standard" results if requested.
        # Otherwise grab all possible links.
        # if not a.parent or a.parent.name.lower() != "h3":
        #     continue

        # Get the URL from the anchor tag.
        try:
            link = a['href']
        except KeyError:
            continue

        # Filter invalid links and links pointing to Google itself.
        url = extract_url(link)
        if not url:
            continue
        else:
            a['href'] = url
            a['target'] = "_blank"
    return soup.find("div", id="search")


class Pagination(object):
    def __init__(self, search_type, keywords, current_page):
        self.search_type = search_type
        self.keywords = keywords
        self.current_page = current_page
        self.pagelinks = self.gen_pagelinks()
        self.previous = self._make_page_url(self.current_page-1 or 1)
        self.next = self._make_page_url(self.current_page + 1)
        self.previous_disabled = "disabled" if self.current_page <= 1 else ""

    def gen_pagelinks(self):
        rv_list = []
        start = max(1, self.current_page-4)
        stop = start + 10
        for page in range(start, stop):
            url = self._make_page_url(page)
            page_link = PageLink(url, page)
            if page == self.current_page:
                page_link.active = "active"
            rv_list.append(page_link)
        return rv_list

    def _make_page_url(self, page):
        query_dic = {"keywords": self.keywords.encode('utf-8'), "page": page}
        return url_for("search", search_type=self.search_type, **query_dic)


class PageLink(object):

    def __init__(self, url, page):
        self.url = url
        self.page = page
        self.active = ""


def mkcontex(keywords, page=1, search_type=None):
    rv_dic = {}
    rv_dic["div"] = Markup(get_page(keywords, page, search_type))
    rv_dic["value"] = keywords
    rv_dic["pagination"] = Pagination(search_type, keywords, current_page=page)
    return rv_dic

@app.route('/')
def index():
    return search("search")


@app.route('/<search_type>')
def search(search_type):
    if search_type == "images":
        context = {"glyphicon": "glyphicon-picture", "images_active": "active", "action": "/images", "button": "Images"}
    else:
        context = {"glyphicon": "glyphicon-search", "search_active": "active", "action": "/search", "button": "Google"}

    keywords = request.form.get("keywords") or request.args.get("q") or request.args.get("keywords")
    page = request.args.get("page", 1)
    try:
        page = int(page)
        assert page >= 1
    except Exception:
        page = 1
    if keywords:
        keywords = ' '.join(keywords.strip().split())
        ext_context = mkcontex(keywords, page, search_type)
        context.update(ext_context)
        return render_template("result.html", **context)
    return render_template("index.html", **context)


if __name__ == '__main__':
    app.debug = True
    app.run(host="127.0.0.1", port=8051)
